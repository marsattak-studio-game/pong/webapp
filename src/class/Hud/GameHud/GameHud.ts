import Game from '../../Containers/Game';
import Text from '../Text';
import AHud from '../AHud';

class GameHud extends AHud {
	constructor(defaultWidth: number, defaultHeight: number) {
		super();
		this._createElements(defaultWidth, defaultHeight);
	}

	protected _createElements(
		defaultWidth: number,
		defaultHeight: number,
	): void {
		this._elements.LIFE_PLAYER_1 = new Text({
			text: 'PONG:',
			x: defaultWidth / 2,
			y: defaultHeight / 2,
			size: 'XL',
		});
		this._elements.LIFE_PLAYER_1.move({
			x: -this._elements.LIFE_PLAYER_1.width / 2,
			y:
				-this._elements.LIFE_PLAYER_1.height / 2 -
				this._elements.LIFE_PLAYER_1.height / 2,
		});

		this._elements.LIFE_PLAYER_2 = new Text({
			text: 'The Game:',
			x: defaultWidth / 2,
			y: defaultHeight / 2,
			size: 'MD',
		});
		this._elements.LIFE_PLAYER_2.move({
			x: -this._elements.LIFE_PLAYER_2.width / 2,
			y:
				-this._elements.LIFE_PLAYER_2.height / 2 +
				this._elements.LIFE_PLAYER_2.height / 2,
		});
	}

	public update(
		container: Game,
		widthScreenRatio: number,
		heightScreenRatio: number,
	): void {
		super._update(widthScreenRatio, heightScreenRatio);
	}
}

export default GameHud;
