import { IHudNamedElement } from './AHud.interfaces';

abstract class AHud {
	protected _lastWidthScreenRation = 0;
	protected _lastHeightScreenRation = 0;
	protected _elements: IHudNamedElement = {};

	protected abstract _createElements(
		defaultWidth: number,
		defaultHeight: number,
	): void;

	public getNamedElements(): IHudNamedElement {
		return this._elements;
	}

	protected _update(
		widthScreenRatio: number,
		heightScreenRatio: number,
	): void {
		if (
			this._lastHeightScreenRation !== heightScreenRatio ||
			this._lastWidthScreenRation !== widthScreenRatio
		) {
			this._lastHeightScreenRation = heightScreenRatio;
			this._lastWidthScreenRation = widthScreenRatio;

			Object.values(this._elements).forEach(elt => {
				elt.update(widthScreenRatio, heightScreenRatio);
			});
		}
	}
}

export default AHud;
