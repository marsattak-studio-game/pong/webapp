import Text from '../Text';

type THudElement = Text;

export interface IHudNamedElement {
	[date: string]: THudElement;
}
