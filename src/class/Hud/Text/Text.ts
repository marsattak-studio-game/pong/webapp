import { Text as PixiText } from 'pixi.js';
import { ITextConstructor } from './Text.interfaces';
import { parseFontSize } from './Text.utils';

class Text extends PixiText {
	private _defaultX: number;
	private _defaultY: number;
	private _defaultWidth: number;
	private _defaultHeight: number;

	constructor({ text, x, y, size }: ITextConstructor) {
		super(text, {
			fontSize: parseFontSize(size || 'MD'),
		});
		this._defaultX = x;
		this._defaultY = y;
		this.position.set(x, y);
		this._defaultWidth = this.width;
		this._defaultHeight = this.height;
	}

	public move({ x, y }: { x?: number; y?: number }): void {
		if (x) {
			this._defaultX += x;
		}
		if (y) {
			this._defaultY += y;
		}
	}

	public update(widthScreenRatio: number, heightScreenRatio: number): void {
		this.position.set(
			this._defaultX * widthScreenRatio,
			this._defaultY * heightScreenRatio,
		);
		this.width = this._defaultWidth * widthScreenRatio;
		this.height = this._defaultHeight * heightScreenRatio;
	}
}

export default Text;
