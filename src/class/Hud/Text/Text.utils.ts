import { TFontSize } from './Text.interfaces';

export function parseFontSize(size: TFontSize): number {
	switch (size) {
		case 'XS': {
			return 20;
		}
		case 'SM': {
			return 40;
		}
		case 'MD': {
			return 60;
		}
		case 'LG': {
			return 80;
		}
		case 'XL': {
			return 100;
		}
	}
}
