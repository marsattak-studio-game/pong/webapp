export type TFontSize = 'XS' | 'SM' | 'MD' | 'LG' | 'XL';

export interface ITextConstructor {
	text: string;
	x: number;
	y: number;
	size?: TFontSize;
}
