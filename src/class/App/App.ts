import { Application } from 'pixi.js';

import appConfig from '../../config/app';
import { IAppContainersList, TKeyBoardType, TAppState } from './App.interfaces';
import Game from '../Containers/Game';

class App {
	private _containers: IAppContainersList;
	public app: Application;
	public defaultWidth: number;
	public defaultHeight: number;
	private _screenElement: HTMLElement;
	private _pCallbackEvent: (eventType: TKeyBoardType, event: KeyboardEvent) => void;
	private _appState: TAppState;

	constructor() {
		this.app = new Application(appConfig);
		this.defaultWidth = appConfig.width;
		this.defaultHeight = appConfig.height;
		this._init();
		window.addEventListener('resize', () => {
			this._resize();
		});
	}

	/**
	 * @private getScreenElement - get screen element
	 * @memberof App
	 */
	private getScreenElement(): HTMLElement {
		let screenElt = document.getElementById('game-screen');
		if (!screenElt) {
			const elt = document.createElement('div');
			elt.id = 'game-screen';
			screenElt = elt;
		}
		screenElt.style.width = '100%';
		screenElt.style.height = '100%';
		screenElt.style.display = 'flex';
		return screenElt;
	}

	private _initEvent(): void {
		window.addEventListener('keydown', (e) => this.pDownListener(e));
		window.addEventListener('keyup', (e) => this.pUpListener(e));
		this._pCallbackEvent = null;
	}

	private _pEvents(eventType: TKeyBoardType, event: KeyboardEvent): void {
		this._pCallbackEvent(eventType, event);
	}

	private pDownListener(event: KeyboardEvent): void {
		this._pEvents('DOWN', event);
	}

	private pUpListener(event: KeyboardEvent): void {
		this._pEvents('UP', event);
	}

	/**
	 * @private _init - initialize the application
	 * @memberof App
	 */
	private _init(): void {
		this._screenElement = this.getScreenElement();
		this.app.view.style.margin = 'auto';
		this._resize();
		this._initEvent();
		this._createContainers();
		this.changeBackgroundColor(0xffffff);
		this.app.stage = this._containers.game.getContainer();
		this._appState = 'game';
	}

	/**
	 * @private _createContainers - Create all containers of the application
	 * @memberof App
	 */
	private _createContainers(): void {
		this._containers = {
			game: new Game(this)
		};
	}

	/**
	 * start - Start the application
	 * @memberof App
	 */
	public start(): void {
		this._pCallbackEvent = (eventType, event): void => this._containers.game.events(eventType, event);
		this._screenElement.appendChild(this.app.view);
		this.gameLoop();
	}

	private _getAppState(): void {
		if (this._appState === 'game' && this._containers.game.isFinish()) {
			// Restart game when it's finish
			this._containers.game = new Game(this);
		}
	}

	gameLoop(): void {
		let oldTime = Date.now();
		const animate = (): void => {
			const newTime = Date.now();
			let deltaTime = newTime - oldTime;
			oldTime = newTime;
			if (deltaTime < 0) deltaTime = 0;
			if (deltaTime > 1000) deltaTime = 1000;
			// const deltaFrame = deltaTime * 60 / 1000; //1.0 is for single frame
			this._getAppState();

			this._containers.game.gameLoop(
				this.app.renderer,
				this.getRendererWidthRatio(),
				this.getRendererHeightRatio()
			);

			requestAnimationFrame(animate);
		};
		animate();
	}

	/**
	 * changeBackgroundColor - Change the background color of the application
	 * @param color
	 */
	public changeBackgroundColor(color = 0x000000): void {
		this.app.renderer.backgroundColor = color;
	}

	public getRendererWidth(): number {
		return this.app.renderer.view.width;
	}

	public getRendererHeight(): number {
		return this.app.renderer.view.height;
	}

	public setRendererWidth(width = this.getRendererWidth()): void {
		this.app.renderer.resize(width, this.getRendererHeight());
	}

	public setRendererHeight(height = this.getRendererHeight()): void {
		this.app.renderer.resize(this.getRendererWidth(), height);
	}

	public getRendererWidthRatio(): number {
		return this.app.renderer.view.width / this.defaultWidth;
	}

	public getRendererHeightRatio(): number {
		return this.app.renderer.view.height / this.defaultHeight;
	}

	private _resize(): void {
		if (window.innerWidth / window.innerHeight < this.defaultWidth / this.defaultHeight) {
			this.app.renderer.resize(window.innerWidth, window.innerWidth / (this.defaultWidth / this.defaultHeight));
		} else {
			this.app.renderer.resize(window.innerHeight * (this.defaultWidth / this.defaultHeight), window.innerHeight);
		}
	}
}

export default App;
