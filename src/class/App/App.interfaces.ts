import Game from '../Containers/Game';

type TAppState = 'game';

interface IAppContainersList {
	game: Game;
}

type TKeyBoardType = 'DOWN' | 'UP';

export { IAppContainersList, TKeyBoardType, TAppState };
