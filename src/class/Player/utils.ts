import { TBonusType } from '../Bonus';
import { IPlayerBonus } from './Player.interfaces';

/**
 * createBonus - Generate a bonus for the player
 *
 * @param {TBonusType} [type='HIT']
 * @returns {IPlayerBonus}
 */
function createBonus(type: TBonusType = 'HIT'): IPlayerBonus {
	switch (type) {
		case 'SHOOT':
			return {
				type,
				animation: {
					status: 'READY',
					value: 3,
					canActivate: true
				}
			};
			break;

		case 'HIT':
			return {
				type,
				animation: {
					status: 'READY',
					value: 0,
					canActivate: true
				}
			};
			break;
	}
}

export { createBonus };
