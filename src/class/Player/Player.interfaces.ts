import { TBonusType } from '../Bonus';

type TPlayerMove = 'TOP' | 'BOTTOM' | 'NONE';

/**
 * @interface IPLayerConstructor - Player constructor object
 * 
 * @member {number} [x]
 * @member {number} [y]
 * @member {number} [width]
 * @member {number} [height]
 * 
 * @returns {IPlayerBonus} [IPlayerBonus]
 */
interface IPLayerConstructor {
	/** @x {number} - The X position of the Player */
	x: number;
	/** @y {number} - The Y position of the Player */
	y: number;
	/** @width {number} - The Width position of the Player */
	width: number;
	/** @height {number} - The Height position of the Player */
	height: number;
	/** @direction {TPlayerDirection} - The Player direction ( LEFT || RIGHT) */
	direction: TPlayerDirection;
}

type TBonusStatus = 'READY' | 'PENDING' | 'FINISH';

/**
 * @type {TPlayerDirection} ['LEFT' | 'RIGHT'] - Direction of the player
 */
type TPlayerDirection = 'LEFT' | 'RIGHT';

interface IPlayerBonusAnimation {
	/** @status {TBonusStatus} - The Animation Status */
	status: TBonusStatus;
	/** @value {number} -  The value to the animation*/
	value: number;
	/** @canActivate {boolean} - If bonus animation can activate */
	canActivate: boolean;
}

/**
 * Player Bonus Object
 * @interface IPlayerBonus
 * 
 * @member {TBonusType} [type] - The bonus type
 * @member {IPlayerBonusAnimation} [animation] - Animation object
 * @member {nextBonus} {nextBonus} - Next bonus to add
 */
interface IPlayerBonus {
	/** @type {TBonusType} - The bonus type */
	type: TBonusType;
	/** @animation {IPlayerBonusAnimation} - Animation object */
	animation: IPlayerBonusAnimation;
	/** @nextBonus {TBonusType} - Next bonus to add */
	nextBonus?: TBonusType;
}

export { IPLayerConstructor, IPlayerBonus, TPlayerDirection, TPlayerMove };
