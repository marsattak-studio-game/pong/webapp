import { Graphics } from 'pixi.js';
import { IPLayerConstructor, IPlayerBonus, TPlayerDirection, TPlayerMove } from './Player.interfaces';
import ACollisionableObject from '../GameObjects/ACollisionableObject';
import { TBonusType } from '../Bonus';
import { createBonus } from './utils';
import Bullet from '../Bullet';

class Player extends ACollisionableObject {
	private _moveTop: boolean;
	private _moveBottom: boolean;
	private _bullets: Bullet[] = [];
	private _bonus: IPlayerBonus;
	private _verticalMove: number;
	private _direction: TPlayerDirection;
	private _timestampImpacted: number;
	private _timeImacted: number;
	private _isImpacted: boolean;

	constructor({ x, y, width, height, direction }: IPLayerConstructor) {
		super(x, y, width, height);
		this._moveTop = false;
		this._moveBottom = false;
		this._direction = direction;
		this._bonus = createBonus('HIT');
		this._verticalMove = 10;
		this._timestampImpacted = 0;
		this._timeImacted = 0;
		this._isImpacted = false;
	}

	public getMove(): TPlayerMove {
		if (this._moveTop) return 'TOP';
		if (this._moveBottom) return 'BOTTOM';
		return 'NONE';
	}

	public moveTop(move = true): void {
		this._moveTop = move;
	}

	public moveBottom(move = true): void {
		this._moveBottom = move;
	}

	public action(active = true): void {
		if (active) {
			if (this._bonus.animation.status === 'READY' && this._bonus.animation.canActivate) {
				this._bonus.animation.status = 'PENDING';
				this._bonus.animation.canActivate = false;
			}
		} else {
			if (!this._bonus.animation.canActivate) {
				this._bonus.animation.canActivate = true;
			}
		}
	}

	public getBonus(): IPlayerBonus {
		return this._bonus;
	}

	public getBullets(): Bullet[] {
		return this._bullets;
	}

	public setBullets(bullets: Bullet[]): void {
		this._bullets = bullets;
	}

	public impacted(time = 1000): void {
		this._timestampImpacted = Date.now();
		this._timeImacted = time;
		this._isImpacted = true;
	}

	public addBonus(type: TBonusType): void {
		if (this._bonus.animation.status === 'PENDING') {
			this._bonus.nextBonus = type;
		} else {
			this._bonus = createBonus(type);
		}
	}

	public collisions(objects: ACollisionableObject[] = []): void {
		objects.map((obj) => {
			if (this._moveBottom && this.isInCollisionToBottom(obj)) {
				this.setY(obj.getCollisionRect().top - this.getHeight());
			} else if (this._moveTop && this.isInCollisionToTop(obj)) {
				this.setY(obj.getCollisionRect().bottom);
			}
		});
	}

	public actionAnimation(): void {
		if (this._bonus.animation.status === 'FINISH') {
			if (this._bonus.nextBonus) {
				this._bonus = createBonus(this._bonus.nextBonus);
			} else {
				this._bonus.animation.status = 'READY';
			}
		}
		if (this._bonus.animation.status === 'PENDING') {
			switch (this._bonus.type) {
				case 'SHOOT': {
					const right = this._direction === 'RIGHT';
					this._bonus.animation.value -= 1;
					this._bullets.push(
						new Bullet({
							x: this.getX() + (right ? this.getWidth() : -this.getWidth()),
							y: this.getY() + this.getHeight() / 3,
							size: 32,
							direction: this._direction
						})
					);
					if (this._bonus.animation.value === 0) {
						this._bonus = createBonus('HIT');
					} else {
						this._bonus.animation.status = 'READY';
					}
					break;
				}
				case 'HIT': {
					let xAnim = this._direction === 'RIGHT' ? 4 : -4;
					// Default action is hit - HIT
					if (this._bonus.animation.value < 20) {
						this._bonus.animation.value += 2;
					} else {
						this._bonus.animation.value += 2;
						xAnim = this._direction === 'RIGHT' ? -4 : 4;
					}
					this.setX(this.getX() + xAnim);
					if (this._bonus.animation.value === 40) {
						this._bonus.animation.status = 'FINISH';
						this._bonus.animation.value = 0;
					}
					break;
				}
			}
		}
	}

	public update(): void {
		if (this._isImpacted) {
			const now = Date.now();
			if (now - this._timestampImpacted > this._timeImacted) {
				this._timestampImpacted = 0;
				this._timeImacted = 0;
				this._isImpacted = false;
			}
		} else {
			this.actionAnimation();
			if (this._moveTop) {
				this.setY(this.getY() - this._verticalMove);
			}
			if (this._moveBottom) {
				this.setY(this.getY() + this._verticalMove);
			}
		}
		this._bullets.forEach((bullet) => {
			bullet.update();
		});
	}

	/**
	 * draw - TODO
	 * @memberof Player
	 */
	public draw(graphic: Graphics, widthScreenRation: number, heightScreenRatio: number): void {
		graphic.beginFill(0x5cafe2);
		if (this._bonus.type === 'SHOOT') {
			const right = this._direction === 'RIGHT';
			graphic.drawRect(
				(this.getX() + (right ? this.getWidth() : -this.getWidth())) * widthScreenRation,
				(this.getY() + this.getHeight() / 3) * heightScreenRatio,
				this.getWidth() * widthScreenRation,
				(this.getHeight() - this.getHeight() / 3 * 2) * heightScreenRatio
			);
		}
		graphic.drawRect(
			this.getX() * widthScreenRation,
			this.getY() * heightScreenRatio,
			this.getWidth() * widthScreenRation,
			this.getHeight() * heightScreenRatio
		);
		// Collisions
		this._bullets.forEach((bullet) => {
			bullet.draw(graphic, widthScreenRation, heightScreenRatio);
		});
		graphic.endFill();
	}
}

export default Player;
