interface IWallConstructor {
	x: number;
	y: number;
	width: number;
	height: number;
}

export { IWallConstructor };
