import { IWallConstructor } from './Wall.interfaces';
import ACollisionableObject from '../GameObjects/ACollisionableObject';

class Wall extends ACollisionableObject {
	constructor({ x, y, width, height }: IWallConstructor) {
		super(x, y, width, height);
	}
}

export default Wall;
