import { Graphics, utils } from 'pixi.js';
import { IBonusConstructor, TBonusType } from './Bonus.interfaces';
import ACollisionableObject from '../GameObjects/ACollisionableObject';
import Player from '../Player';
import { getRandomInt } from '../../utils';

class Bonus extends ACollisionableObject {
	private _horizontalMove: number;
	private _verticalMove: number;
	private _type: TBonusType;
	private _toRemove: boolean;

	constructor({ x, y, size, type }: IBonusConstructor) {
		super(x, y, size, size);
		this._horizontalMove = -5 + getRandomInt(10);
		this._verticalMove = -5 + getRandomInt(10);
		this._type = type;
		this._toRemove = false;
	}

	public toRemove(): boolean {
		return this._toRemove;
	}

	public update(): void {
		if (this._verticalMove !== 0) {
			this.setY(this.getY() + this._verticalMove);
		}
		if (this._horizontalMove !== 0) {
			this.setX(this.getX() + this._horizontalMove);
		}
	}

	public collisions(objects: ACollisionableObject[] = []): void {
		objects.map((obj) => {
			const ifIsAPlayer = obj instanceof Player;
			if (this._verticalMove > 0 && this.isInCollisionToBottom(obj)) {
				this._verticalMove = -this._verticalMove;
				this.setY(this.getY() + this._verticalMove);
			} else if (this._verticalMove < 0 && this.isInCollisionToTop(obj)) {
				this._verticalMove = -this._verticalMove;
				this.setY(this.getY() + this._verticalMove);
			}

			if (this._horizontalMove < 0 && this.isInCollisionToLeft(obj)) {
				if (ifIsAPlayer) {
					(obj as Player).addBonus(this._type);
					this._toRemove = true;
				} else {
					this._horizontalMove = -this._horizontalMove;
					this.setX(this.getX() + this._horizontalMove);
				}
			} else if (this._horizontalMove > 0 && this.isInCollisionToRight(obj)) {
				if (ifIsAPlayer) {
					(obj as Player).addBonus(this._type);
					this._toRemove = true;
				} else {
					this._horizontalMove = -this._horizontalMove;
					this.setX(this.getX() - this._horizontalMove);
				}
			}
		});
	}

	/**
	 * draw - TODO
	 * @memberof Bonus
	 */
	public draw(graphic: Graphics, widthScreenRation: number, heightScreenRatio: number): void {
		graphic.beginFill(utils.rgb2hex([ 255, 0, 255 ]));
		const realWidth = this.getWidth() / 2;
		const realHeight = this.getHeight() / 2;
		const realX = this.getX() + realWidth;
		const realY = this.getY() + realHeight;
		graphic.drawEllipse(
			realX * widthScreenRation,
			realY * heightScreenRatio,
			realWidth * widthScreenRation,
			realHeight * heightScreenRatio
		);
		graphic.endFill();
	}
}

export default Bonus;
