type TBonusType = 'HIT' | 'SHOOT';

interface IBonusConstructor {
	x: number;
	y: number;
	size: number;
	type: TBonusType;
}

export { TBonusType, IBonusConstructor };
