import { Graphics, utils } from 'pixi.js';
import { IBulletConstructor, TBulletType } from './Bullet.interfaces';
import ACollisionableObject from '../GameObjects/ACollisionableObject';
import Player from '../Player';

class Bullet extends ACollisionableObject {
	private _horizontalMove: number;
	private _type: TBulletType;
	private _toRemove: boolean;

	constructor({ x, y, size, direction }: IBulletConstructor) {
		super(x, y, size, size);
		this._horizontalMove = direction === 'LEFT' ? -10 : 10;
		this._toRemove = false;
	}

	public toRemove(): boolean {
		return this._toRemove;
	}

	public update(): void {
		if (this._horizontalMove !== 0) {
			this.setX(this.getX() + this._horizontalMove);
		}
	}

	public collisions(objects: ACollisionableObject[] = []): void {
		objects.map((obj) => {
			const ifIsAPlayer = obj instanceof Player;
			if (this._horizontalMove < 0 && this.isInCollisionToLeft(obj)) {
				if (ifIsAPlayer) {
					(obj as Player).addBonus(this._type);
					(obj as Player).impacted();
				}
				this._toRemove = true;
			} else if (this._horizontalMove > 0 && this.isInCollisionToRight(obj)) {
				if (ifIsAPlayer) {
					(obj as Player).addBonus(this._type);
					(obj as Player).impacted();
				}
				this._toRemove = true;
			}
		});
	}

	/**
	 * draw - TODO
	 * @memberof Bullet
	 */
	public draw(graphic: Graphics, widthScreenRation: number, heightScreenRatio: number): void {
		graphic.beginFill(utils.rgb2hex([ 0, 255, 255 ]));
		const realWidth = this.getWidth() / 2;
		const realHeight = this.getHeight() / 2;
		const realX = this.getX() + realWidth;
		const realY = this.getY() + realHeight;
		graphic.drawEllipse(
			realX * widthScreenRation,
			realY * heightScreenRatio,
			realWidth * widthScreenRation,
			realHeight * heightScreenRatio
		);
		graphic.endFill();
	}
}

export default Bullet;
