import { TPlayerDirection } from '../Player';

type TBulletType = 'HIT' | 'SHOOT';

interface IBulletConstructor {
	x: number;
	y: number;
	size: number;
	direction: TPlayerDirection;
}

export { TBulletType, IBulletConstructor };
