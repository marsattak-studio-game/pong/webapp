import { Graphics } from 'pixi.js';
import { IBallConstructor } from './Ball.interfaces';
import ACollisionableObject from '../GameObjects/ACollisionableObject';
import Player from '../Player';

const DEFAULT_SPEED_VALUE = 4;

class Ball extends ACollisionableObject {
	private _horizontalMove: number;
	private _verticalMove: number;
	private _speedRatio: number;
	private _bonusMove: number;

	constructor({ x, y, size }: IBallConstructor) {
		super(x, y, size, size);
		this._horizontalMove = DEFAULT_SPEED_VALUE;
		this._verticalMove = 0;
		this._speedRatio = 2;
		this._bonusMove = 0;
	}

	public update(): void {
		if (this._verticalMove !== 0) {
			this.setY(this.getY() + this._verticalMove * this._speedRatio);
		}
		if (this._horizontalMove !== 0) {
			this.setX(this.getX() + (this._horizontalMove + this._bonusMove) * this._speedRatio);
		}
	}

	public collisions(objects: ACollisionableObject[] = []): void {
		objects.map((obj) => {
			const ifIsAPlayer = obj instanceof Player;
			if (this._verticalMove > 0 && this.isInCollisionToBottom(obj)) {
				this._verticalMove = -this._verticalMove;
				this.setY(this.getY() + this._verticalMove * this._speedRatio);
			} else if (this._verticalMove < 0 && this.isInCollisionToTop(obj)) {
				this._verticalMove = -this._verticalMove;
				this.setY(this.getY() + this._verticalMove * this._speedRatio);
			}

			if (this._horizontalMove < 0 && this.isInCollisionToLeft(obj)) {
				this._horizontalMove = -this._horizontalMove;
				this.setX(this.getX() + this._horizontalMove);
				if (ifIsAPlayer) {
					if (
						(obj as Player).getBonus().type === 'HIT' &&
						(obj as Player).getBonus().animation.status === 'PENDING'
					) {
						this._bonusMove = this._horizontalMove;
					} else {
						this._bonusMove = 0;
					}
					if ((obj as Player).getMove() !== 'NONE') {
						this._verticalMove += (obj as Player).getMove() === 'TOP' ? 2 : -2;
					}
				}
			} else if (this._horizontalMove > 0 && this.isInCollisionToRight(obj)) {
				this._horizontalMove = -this._horizontalMove;
				this.setX(this.getX() - this._horizontalMove);
				if (ifIsAPlayer) {
					if (
						(obj as Player).getBonus().type === 'HIT' &&
						(obj as Player).getBonus().animation.status === 'PENDING'
					) {
						this._bonusMove = this._horizontalMove;
					} else {
						this._bonusMove = 0;
					}
					if ((obj as Player).getMove() !== 'NONE') {
						this._verticalMove += (obj as Player).getMove() === 'TOP' ? -2 : 2;
					}
				}
			}
		});
	}

	/**
	 * draw - TODO
	 * @memberof Ball
	 */
	public draw(graphic: Graphics, widthScreenRation: number, heightScreenRatio: number): void {
		graphic.beginFill(0x000aaa);
		const realWidth = this.getWidth() / 2;
		const realHeight = this.getHeight() / 2;
		const realX = this.getX() + realWidth;
		const realY = this.getY() + realHeight;
		graphic.drawEllipse(
			realX * widthScreenRation,
			realY * heightScreenRatio,
			realWidth * widthScreenRation,
			realHeight * heightScreenRatio
		);
		graphic.endFill();
	}
}

export default Ball;
