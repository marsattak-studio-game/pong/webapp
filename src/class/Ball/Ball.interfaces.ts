interface IBallConstructor {
	x: number;
	y: number;
	size: number;
}

export { IBallConstructor };
