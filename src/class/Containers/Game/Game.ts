import { Container, Graphics, Renderer } from 'pixi.js';
import GameHud from '../../Hud/GameHud';
import Ball from '../../Ball';
import Player from '../../Player';
import Bonus from '../../Bonus';
import App, { TKeyBoardType } from '../../App';
import Wall from '../../Wall';
import Bullet from '../../Bullet';

type TContainerState = 'STARTED' | 'FINISH';

class Game {
	private container: Container;
	private graphic: Graphics;
	private _hud: GameHud;
	private ball: Ball;
	private _walls: Wall[] = [];
	private _players: Player[] = [];
	private _defaultWidth: number;
	private _defaultHeight: number;
	private _containerState: TContainerState;
	private _bonus: Bonus | null;
	private _bonusTimeBeforeActivate: number;

	constructor(app: App) {
		this._defaultWidth = app.defaultWidth;
		this._defaultHeight = app.defaultHeight;
		this._init();
	}

	public detroy(): void {
		this.container.destroy();
		this.graphic.destroy();
	}

	private _init(): void {
		this.graphic = new Graphics();
		this.container = new Container();
		this._bonus = null;
		this._bonusTimeBeforeActivate = Date.now();
		this.ball = new Ball({
			x: this._defaultWidth / 2 - 32 / 2,
			y: this._defaultHeight / 2 - 32 / 2,
			size: 32,
		});
		this._walls.push(
			new Wall({
				x: 0,
				y: -32 * 4,
				width: this._defaultWidth,
				height: 32 * 4,
			}),
		);
		this._walls.push(
			new Wall({
				x: 0,
				y: this._defaultHeight,
				width: this._defaultWidth,
				height: 32 * 4,
			}),
		);
		this._players.push(
			new Player({
				x: 0,
				y: this._defaultHeight / 2 - 32 * 2,
				width: 32,
				height: 32 * 4,
				direction: 'RIGHT',
			}),
			new Player({
				x: this._defaultWidth - 32,
				y: this._defaultHeight / 2 - 32 * 2,
				width: 32,
				height: 32 * 4,
				direction: 'LEFT',
			}),
		);
		this.container.addChild(this.graphic);

		this._hud = new GameHud(this._defaultWidth, this._defaultHeight);

		Object.values(this._hud.getNamedElements()).forEach(elt => {
			this.container.addChild(elt);
		});
		this._containerState = 'STARTED';
	}

	public isFinish(): boolean {
		return this._containerState === 'FINISH';
	}

	private _cleanScene(): void {
		if (
			this._bonus?.toRemove() ||
			this._bonus?.getCollisionRect().left < 0 ||
			this._bonus?.getCollisionRect().right > this._defaultWidth
		) {
			this._bonus = null;
			this._bonusTimeBeforeActivate = Date.now();
		}

		let newBullets: Bullet[] = [];
		this._players[0].getBullets().forEach(bullet => {
			if (!bullet.toRemove()) {
				newBullets.push(bullet);
			}
		});
		this._players[0].setBullets(newBullets);
		newBullets = [];
		this._players[1].getBullets().forEach(bullet => {
			if (!bullet.toRemove()) {
				newBullets.push(bullet);
			}
		});
		this._players[1].setBullets(newBullets);
	}

	gameLoop(
		renderer: Renderer,
		widthScreenRation: number,
		heightScreenRatio: number,
	): void {
		this._cleanScene();
		this._hud.update(this, widthScreenRation, heightScreenRatio);

		if (
			this.ball.getCollisionRect().left < 0 ||
			this.ball.getCollisionRect().right > this._defaultWidth
		) {
			this._containerState = 'FINISH';
		} else {
			this.phisicalEngine();
			this.graphicEngine(widthScreenRation, heightScreenRatio);
			renderer.render(this.container);
		}
	}

	public getContainer(): Container {
		return this.container;
	}

	public events(eventType: TKeyBoardType, event: KeyboardEvent): void {
		if (event.keyCode === 38) {
			this._players[1].moveTop(eventType === 'DOWN');
		} else if (event.keyCode === 40) {
			this._players[1].moveBottom(eventType === 'DOWN');
		}
		if (event.keyCode === 37) {
			this._players[1].action(eventType === 'DOWN');
		}

		if (event.keyCode === 87) {
			this._players[0].moveTop(eventType === 'DOWN');
		} else if (event.keyCode === 88) {
			this._players[0].moveBottom(eventType === 'DOWN');
		}
		if (event.keyCode === 83) {
			this._players[0].action(eventType === 'DOWN');
		}
	}

	private phisicalEngine(): void {
		//Update
		if (this._bonus) {
			this._bonus.update();
		} else {
			const now = Date.now();
			if (now - this._bonusTimeBeforeActivate > 1000 * 5) {
				this._bonus = new Bonus({
					x: this._defaultWidth / 2 - 32 / 2,
					y: this._defaultHeight / 2 - 32 / 2,
					size: 32,
					type: 'SHOOT',
				});
			}
		}
		this.ball.update();
		this._players[0].update();
		this._players[1].update();

		this._bonus?.collisions([...this._players, ...this._walls]);
		this.ball.collisions([
			...this._players,
			...this._walls,
			...this._players[0].getBullets(),
			...this._players[1].getBullets(),
		]);
		this._players[0].getBullets().map(bullet => {
			bullet.collisions([this._players[1], this.ball]);
		});
		this._players[1].collisions(this._walls);
		this._players[1].getBullets().map(bullet => {
			bullet.collisions([this._players[0], this.ball]);
		});
	}

	private graphicEngine(
		widthScreenRation: number,
		heightScreenRatio: number,
	): void {
		this.graphic.clear();
		this._bonus?.draw(this.graphic, widthScreenRation, heightScreenRatio);
		this.ball.draw(this.graphic, widthScreenRation, heightScreenRatio);
		this._players[0].draw(
			this.graphic,
			widthScreenRation,
			heightScreenRatio,
		);
		this._players[1].draw(
			this.graphic,
			widthScreenRation,
			heightScreenRatio,
		);
	}
}

export default Game;
