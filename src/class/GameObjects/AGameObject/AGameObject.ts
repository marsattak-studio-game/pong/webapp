abstract class AGameObject {
	private _x: number;
	private _y: number;
	constructor(x: number, y: number) {
		this._x = x;
		this._y = y;
	}
	/**
	 * getX - TODO
	 * @returns {number}
	 * @memberof IObject
	 */
	public getX(): number {
		return this._x;
	}
	/**
	 * getY - TODO
	 * @returns {number}
	 * @memberof IObject
	 */
	public getY(): number {
		return this._y;
	}
	/**
	 * setX - TODO
	 * @param {number} newX
	 * @memberof IObject
	 */
	public setX(newX: number): void {
		this._x = newX;
	}

	/**
	 * setY - TODO
	 * @param {number} newY
	 * @memberof IObject
	 */
	public setY(newY: number): void {
		this._y = newY;
	}
}

export default AGameObject;
