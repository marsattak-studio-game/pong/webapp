import ACollisionableObject from './ACollisionableObject';

export default ACollisionableObject;

export * from './ACollisionableObject.interfaces';
