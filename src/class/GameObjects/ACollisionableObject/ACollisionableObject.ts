import AGameObject from '../AGameObject';
import { ICollisionRect } from './ACollisionableObject.interfaces';

abstract class ICollisionableObject extends AGameObject {
	private _width: number;
	private _height: number;
	constructor(x: number, y: number, width: number, height: number) {
		super(x, y);
		this._width = width;
		this._height = height;
	}

	/**
	 * getCollisionRect - return the collision rect to the object
	 *
	 * @returns {ICollisionRect}
	 * @memberof ICollisionableObject
	 */
	getCollisionRect(): ICollisionRect {
		return {
			left: this.getX(),
			top: this.getY(),
			right: this.getX() + this._width,
			bottom: this.getY() + this._height
		};
	}

	/**
	 * isInCollisionToLeft - Check if the object is in collision to another object when i move to left
	 *
	 * @param {ICollisionableObject} object - to check the collision
	 * @returns {boolean} - is in collision return true
	 * @memberof ICollisionableObject
	 */
	isInCollisionToLeft(object: ICollisionableObject): boolean {
		if (
			object.getCollisionRect().right > this.getCollisionRect().left &&
			object.getCollisionRect().left < this.getCollisionRect().left &&
			((object.getCollisionRect().top < this.getCollisionRect().top &&
				object.getCollisionRect().bottom > this.getCollisionRect().top) ||
				(object.getCollisionRect().top < this.getCollisionRect().bottom &&
					object.getCollisionRect().bottom > this.getCollisionRect().bottom))
		) {
			return true;
		}
		return false;
	}

	/**
	 * isInCollisionToRight - Check if the object is in collision to another object when i move to right
	 *
	 * @param {ICollisionableObject} object - to check the collision
	 * @returns {boolean} - is in collision return true
	 * @memberof ICollisionableObject
	 */
	isInCollisionToRight(object: ICollisionableObject): boolean {
		if (
			object.getCollisionRect().left < this.getCollisionRect().right &&
			object.getCollisionRect().right > this.getCollisionRect().right &&
			((object.getCollisionRect().top < this.getCollisionRect().top &&
				object.getCollisionRect().bottom > this.getCollisionRect().top) ||
				(object.getCollisionRect().top < this.getCollisionRect().bottom &&
					object.getCollisionRect().bottom > this.getCollisionRect().bottom))
		) {
			return true;
		}
		return false;
	}

	/**
	 * isInCollisionToTop - Check if the object is in collision to another object when i move to top
	 *
	 * @param {ICollisionableObject} object - to check the collision
	 * @returns {boolean} - is in collision return true
	 * @memberof ICollisionableObject
	 */
	isInCollisionToTop(object: ICollisionableObject): boolean {
		if (
			object.getCollisionRect().bottom > this.getCollisionRect().top &&
			object.getCollisionRect().top < this.getCollisionRect().top &&
			((object.getCollisionRect().left < this.getCollisionRect().left &&
				object.getCollisionRect().right > this.getCollisionRect().left) ||
				(object.getCollisionRect().left < this.getCollisionRect().right &&
					object.getCollisionRect().right > this.getCollisionRect().right))
		) {
			return true;
		}
		return false;
	}

	/**
	 * isInCollisionToBottom - Check if the object is in collision to another object when i move to bottom
	 *
	 * @param {ICollisionableObject} object - to check the collision
	 * @returns {boolean} - is in collision return true
	 * @memberof ICollisionableObject
	 */
	isInCollisionToBottom(object: ICollisionableObject): boolean {
		if (
			object.getCollisionRect().top < this.getCollisionRect().bottom &&
			object.getCollisionRect().bottom > this.getCollisionRect().bottom &&
			((object.getCollisionRect().left < this.getCollisionRect().left &&
				object.getCollisionRect().right > this.getCollisionRect().left) ||
				(object.getCollisionRect().left < this.getCollisionRect().right &&
					object.getCollisionRect().right > this.getCollisionRect().right))
		) {
			return true;
		}
		return false;
	}

	/**
	 * getWidth - TODO
	 * @returns {number}
	 * @memberof IObject
	 */
	public getWidth(): number {
		return this._width;
	}
	/**
	 * getY - TODO
	 * @returns {number}
	 * @memberof IObject
	 */
	public getHeight(): number {
		return this._height;
	}
	/**
	 * setWidth - TODO
	 * @param {number} newWidth
	 * @memberof IObject
	 */
	public setWidth(newWidth: number): void {
		this._width = newWidth;
	}

	/**
	 * setHeight - TODO
	 * @param {number} newHeight
	 * @memberof IObject
	 */
	public setHeight(newHeight: number): void {
		this._height = newHeight;
	}
}

export default ICollisionableObject;
