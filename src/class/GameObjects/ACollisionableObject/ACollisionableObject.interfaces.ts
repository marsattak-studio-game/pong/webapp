interface ICollisionRect {
	left: number;
	top: number;
	right: number;
	bottom: number;
}

export { ICollisionRect };
