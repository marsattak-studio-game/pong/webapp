const appConfig = {
	width: 1280, // 32 * 100,
	height: 768, // 32 * 80,
	antialias: true,
	transparent: false,
	resolution: 1
};

export default appConfig;
